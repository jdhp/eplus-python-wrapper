=================
EnergyPlusWrapper
=================

EnergyPlus wrapper for Python

Note:

    This project is in beta stage, the API may change.

Contents:

.. toctree::
   :maxdepth: 2

   intro
   API <api>
   developer

* Web site: http://www.jdhp.org/software_en.html#epw
* Online documentation: https://jdhp.gitlab.io/epw
* Source code: https://gitlab.com/jdhp/eplus-python-wrapper
* Issue tracker: https://gitlab.com/jdhp/eplus-python-wrapper/issues
* epw on PyPI: https://pypi.org/project/epw

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Credits
=======

.. include:: ../AUTHORS
   :literal:

License
=======

.. highlight:: none

.. include:: ../LICENSE
   :literal:

